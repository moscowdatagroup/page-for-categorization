import os
import sys
from setuptools import setup

def read(fname):
	sys.path.insert(0, os.path.dirname(__file__))
	return open(os.path.join(os.path.dirname(__file__), fname)).read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(

      name = "categorization_page",
      version = "0.1",

      description = """Page for manual categorization of items""",
      long_description = read('README.rst'),

      author = "Max Kharchenko",
      author_email = "maksim.kharchenko@linio.com",
      install_requires = required,

)