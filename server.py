# coding: utf-8
from flask import Flask, render_template, request, Response, redirect, url_for

from sqlalchemy import create_engine, or_
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime, timedelta

import json
import os


app = Flask(__name__)
app.config.from_object('config.default')
app.config.from_envvar('CAT_PAGE_SETTING', silent=True)
engine = create_engine(app.config['DATABASE_URI'])

db_session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

from models import * 


with open(app.config['CATEGORY_NAMES_MAP'], 'r') as outfile:
    category_names_map = json.load(outfile)

with open(app.config['CATEGORY_SAMPLES_MAP'], 'r') as outfile:
    category_samples_map = json.load(outfile)

def init_db():
	"""Create db via sqlalchemy """
	import models
	Base.metadata.create_all(bind=engine)

@app.route('/')
def show_variant():
	# lock time
	since = datetime.utcnow() - timedelta(minutes=5)

	item = SkuItem.query.filter(or_(SkuItem.status  == '0', SkuItem.status == '-1'))
	item = item.filter(or_(SkuItem.updated_at  == None,  SkuItem.updated_at  <  since)).first()
	
	items_left = SkuItem.query.filter(or_(SkuItem.status  == '0', SkuItem.status == '-1')).count()-1
	
	if item is None:
		context = {'items_left': items_left}
		return render_template('no_data.html', **context)

	context = {
	'item': item,
	'items_left': items_left,
	'category_map': category_names_map,
	'suggest': eval(item.categories_suggest),
	'samples_map': category_samples_map
	}

	# dirty fucking hack to process lock inside updated_at field. Fck you sqlalchemy and no touch 

	if item.status == '0':
		item.status = '-1'
	else:
		item.status = '0'

	db_session.merge(item)

	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return render_template('index.html', **context)

@app.route('/approve_predicted/<sku_name>', methods=['POST'])
def approve_predicted(sku_name):

	item = SkuItem.query.filter(SkuItem.sku == sku_name).first()
	item.status = '1'
	item.approved_category = json.dumps([item.predicted_category])

	db_session.merge(item)
	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return redirect(url_for('show_variant'))

@app.route('/approve_original/<sku_name>', methods=['POST'])
def approve_original(sku_name):

	item = SkuItem.query.filter(SkuItem.sku == sku_name).first()
	item.status = '2'
	item.approved_category = json.dumps([item.original_category])

	db_session.merge(item)

	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return redirect(url_for('show_variant'))

@app.route('/approve_both/<sku_name>', methods=['POST'])
def approve_both(sku_name):

	item = SkuItem.query.filter(SkuItem.sku == sku_name).first()
	item.status = '3'
	item.approved_category = json.dumps([item.original_category, item.predicted_category])

	db_session.merge(item)
	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return redirect(url_for('show_variant'))

@app.route('/bad_item/<sku_name>', methods=['POST'])
def bad_item(sku_name):

	item = SkuItem.query.filter(SkuItem.sku == sku_name).first()
	item.status = '5'
	item.approved_category = json.dumps([])

	db_session.merge(item)
	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return redirect(url_for('show_variant'))

@app.route('/choose_another/<sku_name>', methods=['POST','GET'])
def choose_another(sku_name):

	item = SkuItem.query.filter(SkuItem.sku == sku_name).first()
	item.status = '4'
	item.approved_category = json.dumps([request.args.get('category')])

	db_session.merge(item)
	if "CAT_PAGE_SETTING" in os.environ:
		db_session.commit()
	#db_session.remove()

	return redirect(url_for('show_variant'))


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == '__main__':
    app.run(host=app.config['HOST_NAME'], port=app.config['PORT'],  debug=app.config['DEBUG'])