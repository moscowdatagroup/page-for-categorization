# coding: utf8
import os

from fabric.api import cd, run, local, sudo, put
from fabric.context_managers import settings, shell_env
from fabric.contrib.files import exists, upload_template
from fabric.state import env


# REWRITE WITH NORMAL MANUAL https://realpython.com/blog/python/kickstarting-flask-on-ubuntu-setup-and-deployment/
# FOR NGINX + UNICORN 
# SAMPLE HERE https://github.com/realpython/flask-deploy/blob/master/fabfile.py


env.hosts = ['maksimkh@78.47.238.41']

#def production():
#    """
#    Work on production environment
#    """
#    env.settings = 'production'
#    env.hosts = ['$(production_domain)']
#    env.user = '$(production_user)'
#    env.s3_bucket = '$(production_s3)'
    

def bootstrap(do_pip_install=False):

    _set_env()

    with shell_env(CAT_PAGE_SETTING=env.REMOTE_PROJECT_PATH + 'config/production.py'):
        create_folders()
        create_virtualenv()
        deploy_src()
        pip_install()
        _create_table()
        upload_data()
        configure_dev_server()


def _set_env():

    src_root_path = '/home/maksimkh'
    env.PROJECT_NAME = 'linio_categorization_page'
    env.TGZ_FILENAME = '%s.tar.gz' % env.PROJECT_NAME
    env.REMOTE_PROJECT_PATH = os.path.join(src_root_path, '%s/' % env.PROJECT_NAME)
    env.VIRTUAL_ENV_PATH = os.path.join(src_root_path, '.virtualenvs/%s/' % env.PROJECT_NAME)
    env.PIP = os.path.join(env.VIRTUAL_ENV_PATH, 'bin/pip')
    env.PYTHON = os.path.join(env.VIRTUAL_ENV_PATH, 'bin/python')


def create_folders():
    _mkdir(env.VIRTUAL_ENV_PATH)
    _mkdir(env.REMOTE_PROJECT_PATH)


def _mkdir(path, is_sudo=False):
    runner = sudo if is_sudo else run
    runner('mkdir -p %s' % path)


def create_virtualenv(py3=False):
    if not exists(env.PYTHON):
        if py3:
            run('%s -m venv  %s' % (env.BASE_PYTHON_PATH, env.VIRTUAL_ENV_PATH))
        else:
            run('virtualenv %s' % env.VIRTUAL_ENV_PATH)


def deploy_src():
    local("git archive --output /tmp/{0} HEAD ".format(env.TGZ_FILENAME))
    put("/tmp/%s" % env.TGZ_FILENAME, '/tmp/')
    with cd(env.REMOTE_PROJECT_PATH):
        run("tar xf /tmp/%s" % env.TGZ_FILENAME)


def pip_install(externals=None, is_install_aori_db=False, is_install_aori_db_lq=False, requirements_path=None):
    externals = externals or []
    requirements_path = requirements_path or os.path.join(env.REMOTE_PROJECT_PATH, 'requirements.txt')
    pip_install_cmd_base = '%s install -r %s' % (env.PIP, requirements_path)
    pip_install_cmd = '%s %s' % (
        pip_install_cmd_base,
        ' '.join('--allow-external %s --allow-unverified %s' % (p, p) for p in externals)
    )
    run(pip_install_cmd)


def configure_dev_server():
    upload_template(
        'deploy/linio_categorization_page',
        '/etc/init.d/',
        use_sudo=True,
        context={
            'python': env.PYTHON,
            'project_path': env.REMOTE_PROJECT_PATH,
        },
        backup=True,
    )
    sudo('chmod 777 /etc/init.d/linio_categorization_page')
    with settings(warn_only=True):  # сервис может быть не запущен, не страшно
        _services(['linio_categorization_page'], 'stop')
    _services(['linio_categorization_page'], 'start')


def _services(services_names, action):
    for service in services_names:
        sudo('service %s %s' % (service, action))

def upload_data(filename = 'data/for_check.csv'):
    run('python %supload_data.py %s%s' % (env.REMOTE_PROJECT_PATH, env.REMOTE_PROJECT_PATH, filename))

def _create_table(database='bob_mx_live', user = 'root', password = 'r00t'):
    run('mysql -u %s --password=%s %s < %sschema.sql' % (user, password, database, env.REMOTE_PROJECT_PATH ))


