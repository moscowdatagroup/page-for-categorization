# coding: utf-8
from sqlalchemy import Column, Integer, String, Enum, Text, TIMESTAMP, text
from server import Base
from datetime import datetime

class SkuItem(Base):

    __tablename__ = 'categories_check'
    id = Column(Integer, primary_key=True)
    sku = Column(String(255), unique=True, nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(Text)
    original_category = Column(String(255))
    predicted_category = Column(String(255))
    approved_category = Column(String(255))
    image_url  = Column(String(255))
    categories_suggest = Column(Text)
    updated_at = Column(TIMESTAMP, onupdate=datetime.utcnow)
    status = Column(Enum('-1', '0', '1', '2', '3', '4', '5'),  nullable=False, default='0')



    def __init__(self, sku, name, description=None, original_category=None, predicted_category=None, image_url = None, categories_suggest = None, status = '0'):
        self.sku = sku
        self.name = name
        self.description = description
        self.original_category = original_category
        self.predicted_category = predicted_category
        self.status = status
        self.image_url = image_url
        self.categories_suggest = categories_suggest

    def __repr__(self):
        return '<SkuItem: sku %r, name %r >' % (self.sku, self.name)

