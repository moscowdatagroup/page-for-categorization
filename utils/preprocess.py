# coding: utf-8
import pandas as pd

def change_key_types(name):
	new_dict = {}
	old_dict = eval(name)
	for key in old_dict:
		new_dict[str(key)] = old_dict[key]
	return new_dict

def create_nice_file(filename):

	raw_df = pd.read_csv(filename)
	raw_df['dict_of_other_categories'] = raw_df['dict_of_other_categories'].apply(change_key_types).apply(repr)
	return raw_df

