drop table if exists categories_check;
create table categories_check (
  id integer primary key auto_increment,
  sku varchar(255) not null,
  name varchar(255) not null,
  description text, 
  original_category varchar(255),
  predicted_category varchar(255),
  approved_category varchar(255), 
  image_url varchar(255),
  categories_suggest text,
  updated_at timestamp null,
  status enum('-1', '0', '1', '2', '3', '4', '5') not null default '0',
  unique (sku(255))
);