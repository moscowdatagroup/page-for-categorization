import argparse
import os

from utils.preprocess import *

from models import *
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
# coding: utf-8
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError

if "CAT_PAGE_SETTING" in os.environ:
	from config.production import *
else:
	from config.default import *


engine = create_engine(DATABASE_URI)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

parser = argparse.ArgumentParser(description='Upload files into database')
parser.add_argument('filename', help='Uploaded filename')
args = parser.parse_args()


def upload_preprocessed_file(filename):

	nice_df = create_nice_file(filename)

	for i in nice_df.index:
		item =  nice_df.ix[i]
		item_object = SkuItem(
			sku = item['sku'], 
			name = item['name'],
			description = item['description'],
			original_category = str(item['original_category_id']),
			predicted_category = str(item['predicted_category_id']),
			image_url = item['image'],
			categories_suggest = item['dict_of_other_categories']
			)
		try:
			db_session.add(item_object)
			db_session.commit()
		except IntegrityError:
			print "Duplicate entry: ", item_object
			db_session.rollback()

	db_session.close()




if __name__ == '__main__':
	print args.filename
	upload_preprocessed_file(args.filename)
	engine.dispose()
