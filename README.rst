Page for categorization
-----------------------

Page for manual misclassified items fixing. It used to approve or decline classification algorithm predictions and set labelling.

Install
-------

To install you need simply to do

``python setup.py install``


To run on local server
----------------------

Before run you need to add credentials to config/default.py

``DATABASE_URI = <your_connection_string>``

Then create table in this database 

``mysql -u <user_name> -p <db_name> < schema.sql``

Then preprocess data sample in data/for_check.csv or put here your own file and add it to db with following command

``python upload_data.py <filename>``

After it you will have samples uploaded in DB and you can start Flask application

``python server.py``

Then just open localhost:5000 and enjoy :)


Required data sources
---------------------

Current solution use some additional dictionaries for categories full names mapping and category item samples short info.

``data/category_mapping.py``
``data/category_samples.py``

