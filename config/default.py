DEBUG = True
DATABASE_URI = 'mysql+pymysql://localhost/test?charset=utf8'
CATEGORY_NAMES_MAP = 'data/category_mapping.txt'
CATEGORY_SAMPLES_MAP = 'data/category_samples.txt'
PORT = 5000
HOST_NAME = '0.0.0.0'